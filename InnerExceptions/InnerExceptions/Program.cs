﻿using System;
using System.IO;

namespace InnerExceptions
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                    try
                    {
                        Console.WriteLine("Enter first number: ");
                        int FN = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Enter second number: ");
                        int SN = Convert.ToInt32(Console.ReadLine());

                        int result = FN / SN;

                        Console.WriteLine($"Result equals: {result} ");
                    }
                    catch (Exception ex) // this is inner exception
                    {
                        string filePath = @"C:\Users\Eddy\Desktop\CSharp\Exceptions\Log1.txt";
                        if (File.Exists(filePath))
                        {
                            StreamWriter sw = new StreamWriter(filePath);
                            sw.WriteLine(ex.GetType().Name);
                            sw.WriteLine(ex.Message);
                            sw.Close();
                            Console.WriteLine("There is a problem, Please try later");
                        }
                        else
                        {
                            throw new FileNotFoundException(filePath + " is not present", ex);
                        }

                    }

            }
            catch(Exception exception)
            {
                Console.WriteLine($"Current exception: {exception.GetType().Name}");
                if(exception.InnerException != null)
                {
                    Console.WriteLine($"Inner exception: {exception.InnerException.GetType().Name}");
                }
                
            }
            


           

           
        }
    }
}
