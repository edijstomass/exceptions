﻿using System;
using System.IO;

namespace ExceptionHandling
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader streamReader = null;
            try
            {
                streamReader = new StreamReader(@"C:\Users\Eddy\Desktop\CSharp\Exceptions\Data.txt");
                Console.WriteLine(streamReader.ReadToEnd());
                //streamReader.Close();  needs to transfer to finally, cuz if we get a exception, we haven't released resources
            }
            //catch (Exception ex)
            catch(FileNotFoundException ex)
            {
                //Console.WriteLine(ex.Message);    // sadus message parasti parastam lietotajam nerada
                //Console.WriteLine("\n\n\n");      // bet nosuta back end
                //Console.WriteLine(ex.StackTrace); 

                Console.WriteLine($"Pleace check if the file {ex.FileName} exists");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if(streamReader != null)    // if, cuz if program gets exception, it doesn't creates a new streamReader and gets null error
                {
                    streamReader.Close();
                }
                Console.WriteLine("Finally block");
            }



        }
    }
}
